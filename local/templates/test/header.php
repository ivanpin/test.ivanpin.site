<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
    <!doctype html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
    <head>
        <?
        $APPLICATION->ShowHead();

        use Bitrix\Main\Page\Asset;

        // CSS

        // JS
        CJSCore::Init(array("jquery"));

        //STRING
        Asset::getInstance()->addString("<meta name='viewport' content='width=device-width, initial-scale=1'>");
        Asset::getInstance()->addString("<meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>");
        Asset::getInstance()->addString("<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext' rel='stylesheet'>");
        //Asset::getInstance()->addString('<link rel="shortcut icon" href="/favicon.ico" />');
        ?>
        <title><? $APPLICATION->ShowTitle() ?></title>
    </head>
<body>
<?$APPLICATION->ShowPanel();?>
<?
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
?>
