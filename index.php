<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Тестовое задание");

\Bitrix\Main\Loader::includeModule('iblock');

//echo \Bitrix\Iblock\Iblock::wakeUp(39)->getEntityDataClass();


$cnt = \Bitrix\Iblock\Elements\ElementResidentsTable::getCount();

$nav = new \Bitrix\Main\UI\ReversePageNavigation("index.php", $cnt);
$nav->allowAllRecords(true)
    ->setPageSize(3)
    ->initFromUri();

$ResidentsTable = \Bitrix\Iblock\Elements\ElementResidentsTable::getList(
    array(
        'select' => ['Fio.VALUE', 'Home.ELEMENT.Number.VALUE', 'Home.ELEMENT.Street.VALUE', 'Home.ELEMENT.City.VALUE'],
        'filter' => ['=ACTIVE' => 'Y'],
        "count_total" => true,
        "offset" => $nav->getOffset(),
        "limit" => $nav->getLimit(),
    )
);

$nav->setRecordCount($ResidentsTable->getCount());

while ($Resident = $ResidentsTable->fetch()) {
    $fullAddress = $Resident["IBLOCK_ELEMENTS_ELEMENT_RESIDENTS_Home_ELEMENT_City_VALUE"] . ", " . $Resident["IBLOCK_ELEMENTS_ELEMENT_RESIDENTS_Home_ELEMENT_Street_VALUE"] . ", " . $Resident["IBLOCK_ELEMENTS_ELEMENT_RESIDENTS_Home_ELEMENT_Number_VALUE"];
    $fullAddress = str_replace("  ", "", trim($fullAddress));
    ?>
    <div>-- <?= $Resident["IBLOCK_ELEMENTS_ELEMENT_RESIDENTS_Fio_VALUE"] ?> - <?= $fullAddress ?></div>
<? } ?>

<?
$APPLICATION->IncludeComponent(
    "bitrix:main.pagenavigation",
    "",
    array(
        "NAV_OBJECT" => $nav,
        "SEF_MODE" => "Y",
    ),
    false
);

